console.log("s19 Activity")

// No.  3 - 4

const getCube = Math.pow(2, 3);
console.log(getCube);
// or
const GC = 2**3;
console.log(GC);

console.log(`The cube of 2 is ${getCube}.`)

// No. 5 - 6
const address = [258, "washington", "NW", 90011];

function addressCode(livesIn){
	const[one, two, three, four] = livesIn

	return `I live at ${one} ${two} Ave ${three}, California ${four}`
}

console.log(addressCode(address));

// No. 7 - 8

let animal ={
	
	name: "Lolong", 
	type: "saltwater", 
	weight: "1075",
	measurement: "20 ft"
}

function animalOne(Creature){

let {name, type, weight, measurement} = Creature
	return `${name} was a ${type} crocodile. He weighed at ${weight} kgs with a measurement of ${measurement} 3 in.`
}

console.log(animalOne(animal));

// No. 9 - 10

let nums = [1, 2, 3, 4, 5];

nums.forEach(number => console.log(number))

// 11

let reduceNumber = nums.reduce((a,b) => a + b)
console.log(reduceNumber)

// 12 - 13

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;

	}
}

const thisDog = new Dog("courage", 5, "The cowardly dog");
console.log(thisDog)